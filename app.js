var empname=null;
var empemail=null;
var empskill=null;
var empsal=null;
var numbers = /^[0-9]+$/;
var empArr=[];


function checkdata(employee){
	
	if(empname!=null && empemail!=null && empsal!=null && empskill!=null)
	{
		document.getElementById('addicon').style.color= '#336699';
		console.log("All data is valid");
		console.log("Valid name ",empname);
		console.log("Valid skill",empskill);
		console.log("Valid salary ",empsal);
		console.log("Valid email",empemail);
		
			// adddata.style.color=#336699;
	}

	if(employee.id==="nameid")
	{
		console.log("inside checkname ",employee.value+"length ",employee.value.length);
		if(employee.value.length<8 )
		{
			var error= document.getElementById('nameerror');
			error.innerHTML="Name must contain atleast 8 characters";

		}
		else if(employee.value.length>16)
		{
			var error= document.getElementById('nameerror');
			error.innerHTML="Name cannot exceed 16 characters";
		}
		else if(employee.value.charAt(0)<'a' || employee.value.charAt(0)>'z'   )
		{
			var error= document.getElementById('nameerror');
			error.innerHTML="First letter should be lowercase";
		}
		else if(!(/^[a-zA-Z]+$/.test(employee.value)))
		{
			var error= document.getElementById('nameerror');
			error.innerHTML="Name should not contain digits.";
		}

		else	{
			var error= document.getElementById('nameerror');
			error.innerHTML="";
			empname=employee.value;	
			
		}
	}
	
	else if(employee.id==="skillid")
	{
		console.log("inside check skill ",employee.value+"length ",employee.value.length);
		if(employee.value.length==0 )
		{
			var error= document.getElementById('skillerror');
			error.innerHTML="Enter skill";

		}
		else	{
			var error= document.getElementById('skillerror');
			error.innerHTML="";	
			empskill=employee.value;	
			
		}
		
	}
	else if(employee.id==="salaryid")
	{
		console.log("inside check salary ",employee.value+" length ",employee.value.length);
		if(employee.value<50000)
		{
			var error= document.getElementById('salaryerror');
			error.innerHTML="Minimum salary should be 50000";

		}
		else if(employee.value>100000)
		{
			var error= document.getElementById('salaryerror');
			error.innerHTML="Salary cannot exceed 100000";
		}
		else	{
			var error= document.getElementById('salaryerror');
			error.innerHTML="";	
			empsal=employee.value;	
			
		}
	}
	else if(employee.id==="emailid")
	{
		console.log("inside check email ",employee.value+" length ",employee.value.length);
		
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(employee.value.toLowerCase()))
    {
    	var error= document.getElementById('emailerror');
			error.innerHTML="";	
			empemail=employee.value;	
			
    }
    else
    {
    var error= document.getElementById('emailerror');
			error.innerHTML="Enter valid email";		
    }
	}
	
}

function resetform()	{

	document.getElementById('formid').reset();
	var error= document.getElementById('nameerror');
	error.innerHTML="";	
	var error= document.getElementById('skillerror');
	error.innerHTML="";	
	var error= document.getElementById('salaryerror');
	error.innerHTML="";	
	var error= document.getElementById('emailerror');
	error.innerHTML="";	
	document.getElementById('addicon').style.color= '#ccc';
}
function submitform()	{
	var empObj={
	name:empname,
	email:empemail,
	skill:empskill,
	salary:empsal
	};

	empArr.push(empObj);
	console.log("Afetr pushing in array   ",empArr);

}